package utilities;

/**
 * @author Alessio Addimando
 */

public class ResourceConstant {

    public static final String IMG_BASE = "/resources/images/";
    public static final String AUDIO_BASE = "/resources/audio/";
    public static final String IMG_EXT = ".png";
    public static final String AUDIO_EXT = ".wav";

    public static final String IMG_STATUS_NORMAL = "";
    public static final String IMG_STATUS_ACTIVE = "A";
    public static final String IMG_STATUS_DEAD = "E";
    public static final String IMG_STATUS_IDLE = "F";
    public static final String IMG_STATUS_SUPER = "S";

    public static final String IMG_DIRECTION_SX = "G";
    public static final String IMG_DIRECTION_DX = "D";

    public static final String IMG_CHARACTER_MUSHROOM = "mushroom";
    public static final String IMG_CHARACTER_TURTLE = "turtle";
    public static final String IMG_CHARACTER_MARIO = "mario";

    public static final String IMG_OBJECT_BLOCK = "block";
    public static final String IMG_OBJECT_BLOCK_GOLDEN = "goldenBlock";
    public static final String IMG_OBJECT_COIN1 = "coin1";
    public static final String IMG_OBJECT_COIN2 = "coin2";
    public static final String IMG_OBJECT_TUNNEL = "tunnel";

    public static final String IMG_MARIO_DEFAULT = IMG_BASE + IMG_CHARACTER_MARIO + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_SX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_SUPER + IMG_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MARIO_SUPER_DX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_SUPER + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_SX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_ACTIVE + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_ACTIVE_DX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_ACTIVE + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_SX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_NORMAL + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MARIO_DX = IMG_BASE + IMG_CHARACTER_MARIO + IMG_STATUS_NORMAL + IMG_DIRECTION_DX + IMG_EXT;

    public static final String IMG_MUSHROOM_DX = IMG_BASE + IMG_CHARACTER_MUSHROOM + IMG_STATUS_NORMAL + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MUSHROOM_SX = IMG_BASE + IMG_CHARACTER_MUSHROOM + IMG_STATUS_NORMAL + IMG_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEAD_DX = IMG_BASE + IMG_CHARACTER_MUSHROOM + IMG_STATUS_DEAD + IMG_DIRECTION_DX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEAD_SX = IMG_BASE + IMG_CHARACTER_MUSHROOM + IMG_STATUS_DEAD + IMG_DIRECTION_SX + IMG_EXT;
    public static final String IMG_MUSHROOM_DEFAULT = ResourceConstant.IMG_BASE + ResourceConstant.IMG_CHARACTER_MUSHROOM + ResourceConstant.IMG_STATUS_ACTIVE + IMG_DIRECTION_DX + IMG_EXT;

    public static final String IMG_TURTLE_IDLE = IMG_BASE + IMG_CHARACTER_TURTLE + IMG_STATUS_IDLE + IMG_EXT;
    public static final String IMG_TURTLE_DEAD = IMG_TURTLE_IDLE;

    public static final String IMG_BLOCK = IMG_BASE + IMG_OBJECT_BLOCK + IMG_EXT;
    public static final String IMG_GOLDEN_BLOCK = IMG_BASE + IMG_OBJECT_BLOCK_GOLDEN+ IMG_EXT;

    public static final String IMG_PIECE1 = IMG_BASE + IMG_OBJECT_COIN1 + IMG_EXT;
    public static final String IMG_PIECE2 = IMG_BASE + IMG_OBJECT_COIN2 + IMG_EXT;
    public static final String IMG_TUNNEL = IMG_BASE + IMG_OBJECT_TUNNEL + IMG_EXT;

    public static final String IMG_BACKGROUND = IMG_BASE + "background" + IMG_EXT;
    public static final String IMG_CASTLE = IMG_BASE + "startCastle" + IMG_EXT;
    public static final String START_ICON = IMG_BASE + "start" + IMG_EXT;
    public static final String IMG_CASTLE_FINAL = IMG_BASE + "finalCastle" + IMG_EXT;
    public static final String IMG_FLAG = IMG_BASE + "flag" + IMG_EXT;
    public static final String IMG_GAME_OVER = IMG_BASE + "gameOver" + IMG_EXT;
    public static final String IMG_GAME_WIN= IMG_BASE + "gameWin" + IMG_EXT;


    public static final String AUDIO_MONEY = AUDIO_BASE + "money"+AUDIO_EXT;
    public static final String AUDIO_WIN = AUDIO_BASE +"m"+AUDIO_EXT;
    public static final String AUDIO_JUMP = AUDIO_BASE +"jump"+AUDIO_EXT;

    public static final int MARIO_FREQUENCY = 25;
    public static final int MUSHROOM_FREQUENCY = 45;
    public static final int TURTLE_FREQUENCY = 45;
    public static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    public static final int TURTLE_DEAD_OFFSET_Y = 30;
    public static final int FLAG_X_POS = 4650;
    public static final int CASTLE_X_POS = 4850;
    public static final int FLAG_Y_POS = 115;
    public static final int CASTLE_Y_POS = 145;
    public static final int SCENARIO_LENGTH = 4600;
    public static final int FLIPPING_LENGTH = 800;

}
