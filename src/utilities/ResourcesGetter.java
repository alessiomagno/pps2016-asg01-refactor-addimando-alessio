package utilities;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * @author Alessio Addimando
 */

public class ResourcesGetter {
    public static URL getResource(String path){
        return ResourcesGetter.class.getClass().getResource(path);
    }

    public static Image getImage(String path){
        return new ImageIcon(getResource(path)).getImage();
    }
}
