import gameController.GameLoop;
import gameView.FistLevelStrategy;
import gameView.Platform;

import javax.swing.*;

public class GameStart {

    private static final int WINDOW_WIDTH = 700;
    private static final int WINDOW_HEIGHT = 360;
    private static final String WINDOW_TITLE = "Refactored Super Mario - Assignment 01 ";

    public static void main(String[] args) {
        JFrame windowOfGame = new JFrame(WINDOW_TITLE);
        windowOfGame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        windowOfGame.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        windowOfGame.setLocationRelativeTo(null);
        windowOfGame.setResizable(true);
        windowOfGame.setAlwaysOnTop(true);
        windowOfGame.setContentPane(new Platform(new FistLevelStrategy()));
        windowOfGame.setVisible(true);
        Thread gameLoop = new Thread(new GameLoop());
        gameLoop.start();
    }


}
