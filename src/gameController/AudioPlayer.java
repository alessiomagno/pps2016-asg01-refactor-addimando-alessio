package gameController;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class AudioPlayer {
    private Clip clip;

    public AudioPlayer(String son) {

        try {
            AudioInputStream audio = AudioSystem.getAudioInputStream(getClass().getResource(son));
            clip = AudioSystem.getClip();
            clip.open(audio);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }


    public void play() {
        clip.start();
    }


    public static void playSound(String song) {
        AudioPlayer s = new AudioPlayer(song);
        s.play();
    }
}
