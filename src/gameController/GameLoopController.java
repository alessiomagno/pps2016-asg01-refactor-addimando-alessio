package gameController;

import gameModel.objects.Coin;
import gameModel.objects.GameObject;
import gameView.Platform;
import utilities.ResourceConstant;

import static utilities.ResourceConstant.FLIPPING_LENGTH;
import static utilities.ResourceConstant.SCENARIO_LENGTH;

/**
 * Created by Alessio Addimando on 14/03/2017.
 */
public class GameLoopController {


    private static GameLoopController instance;
    private Platform scene;

    public static GameLoopController getController(){
        if(instance == null){
            instance = new GameLoopController();
        }
        return instance;
    }

    public void addScene(final Platform scene){
        this.scene = scene;
    }

    public Platform getScene() {
        return scene;
    }


    public void controlGame(){

        this.controlContact();
        this.updateBackgroundOnMovement();
        this.animateGameElement();
        this.scene.repaint();
    }

    public void controlContact(){
        for (GameObject object : scene.positionObjects()) {
            if (scene.getMario().isNearToComponent(object))
                scene.getMario().contactWithOtherComponent(object);

            if (scene.getMushroom().isNearToComponent(object))
                scene.getMushroom().contactWithOtherComponent(object);

            if (scene.getTurtle().isNearToComponent(object))
                scene.getTurtle().contactWithOtherComponent(object);
        }

        for (int i = 0; i < scene.positionCoins().size(); i++) {
            if (scene.getMario().contactCoin(scene.positionCoins().get(i))) {
                AudioPlayer.playSound(ResourceConstant.AUDIO_MONEY);
                scene.positionCoins().remove(i);
            }
        }
        if (scene.getMushroom().isNearToComponent(scene.getTurtle())) {
            scene.getMushroom().contactWithOtherComponent(scene.getTurtle());
        }
        if (scene.getTurtle().isNearToComponent(scene.getMushroom())) {
            scene.getTurtle().contactWithOtherComponent(scene.getMushroom());
        }
        if (scene.getMario().isNearToComponent(scene.getMushroom())) {
            scene.getMario().contactWithOtherComponent(scene.getMushroom());
        }
        if (scene.getMario().isNearToComponent(scene.getTurtle())) {
            scene.getMario().contactWithOtherComponent(scene.getTurtle());
        }

    }

    public void updateBackgroundOnMovement() {
        if (scene.getHorizontalPosition() >= 0 && scene.getHorizontalPosition() <= SCENARIO_LENGTH) {
            scene.setHorizontalPosition(scene.getHorizontalPosition()+ scene.getMov());
            scene.setBackground1HorizontalPosition(scene.getBackground1HorizontalPosition() - scene.getMov());
            scene.setBackground2HorizontalPosition(scene.getBackground2HorizontalPosition() - scene.getMov());
            flipBackgrounds();
        }
    }

    public void flipBackgrounds() {
        if (scene.getBackground1HorizontalPosition() == -FLIPPING_LENGTH) {
            scene.setBackground1HorizontalPosition(FLIPPING_LENGTH);
        }
        else if (scene.getBackground2HorizontalPosition() == -FLIPPING_LENGTH) {
            scene.setBackground2HorizontalPosition(FLIPPING_LENGTH);
        }
        else if (scene.getBackground1HorizontalPosition() == FLIPPING_LENGTH) {
            scene.setBackground1HorizontalPosition(-FLIPPING_LENGTH);
        }
        else if (scene.getBackground2HorizontalPosition() == FLIPPING_LENGTH) {
            scene.setBackground2HorizontalPosition(-FLIPPING_LENGTH);
        }

    }

    public void animateGameElement(){
        if (scene.getHorizontalPosition() >= 0 && scene.getHorizontalPosition() <= SCENARIO_LENGTH) {
            for (GameObject object : scene.positionObjects()) {
                object.move();
            }

            for (Coin coin : scene.positionCoins()) {
                coin.move();
            }
            scene.getMushroom().move();
            scene.getTurtle().move();
        }
    }
}
