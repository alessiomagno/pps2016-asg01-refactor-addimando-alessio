package gameView;

import gameController.*;
import gameModel.*;
import gameModel.characters.*;
import gameModel.objects.*;
import utilities.*;

import javax.swing.*;
import java.awt.*;
import java.util.List;

import static utilities.ResourceConstant.*;

public class Platform extends JPanel {

    private final GameElementFactory factory = GameElementConcreteFactory.getInstance();
    private Image imgBackground1;
    private Image imgBackground2;
    private Image imageStartCastle;
    private Image startArrow;
    private Image imageFlag;
    private Image imageFinalCastle;
    private int background1HorizontalPosition;
    private int background2HorizontalPosition;
    private int mov;
    private int xPos;
    private int distanceVerticalFromFloor;
    private int heightLimit;
    private Mario mario = Mario.getInstance();
    private Turtle turtle;
    private Mushroom mushroom;
    private List<GameObject> objects;
    private List<Coin> coins;
    private LevelStrategy strategy;

    public Platform(LevelStrategy strategy) {
        this.background1HorizontalPosition = -50;
        this.background2HorizontalPosition = 750;
        this.mov = 0;
        this.xPos = -1;
        this.distanceVerticalFromFloor = 293;
        this.heightLimit = 0;
        this.imgBackground1 = ResourcesGetter.getImage(ResourceConstant.IMG_BACKGROUND);
        this.imgBackground2 = ResourcesGetter.getImage(ResourceConstant.IMG_BACKGROUND);
        this.imageStartCastle = ResourcesGetter.getImage(ResourceConstant.IMG_CASTLE);
        this.startArrow = ResourcesGetter.getImage(ResourceConstant.START_ICON);
        this.imageFinalCastle = ResourcesGetter.getImage(ResourceConstant.IMG_CASTLE_FINAL);
        this.imageFlag = ResourcesGetter.getImage(ResourceConstant.IMG_FLAG);
        this.setFocusable(true);
        this.requestFocusInWindow();
        this.addKeyListener(new Keyboard());
        this.positionEnemy();
        this.setStrategy(strategy);
        this.configureObjects();
        this.configureCoin();
        GameLoopController.getController().addScene(this);

    }


    public int getDistanceVerticalFromFloor() {
        return distanceVerticalFromFloor;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getHorizontalPosition() {
        return xPos;
    }

    public Mario getMario() {
        return this.mario;
    }

    public Mushroom getMushroom() {
        return this.mushroom;
    }

    public Turtle getTurtle() {
        return this.turtle;
    }

    public int getBackground1HorizontalPosition() {
        return background1HorizontalPosition;
    }

    public int getBackground2HorizontalPosition() {
        return background2HorizontalPosition;
    }

    public void setBackground2HorizontalPosition(int background2HorizontalPosition) {
        this.background2HorizontalPosition = background2HorizontalPosition;
    }

    public void setDistanceVerticalFromFloor(int distanceVerticalFromFloor) {
        this.distanceVerticalFromFloor = distanceVerticalFromFloor;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setHorizontalPosition(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1HorizontalPosition(int x) {
        this.background1HorizontalPosition = x;
    }

    public void setStrategy(LevelStrategy strategy) {
        this.strategy = strategy;
    }

    public void positionEnemy() {
        mushroom = this.factory.createMushroom(800, 263);
        turtle = this.factory.createTurtle(1600, 243);
    }

    public void configureCoin() {
        this.coins = this.strategy.positionCoins();

    }

    public void configureObjects() {
        this.objects = this.strategy.positionObject();

    }

    public List<GameObject> positionObjects() {

        return this.objects;
    }

    public List<Coin> positionCoins() {

        return this.coins;
    }

    @Override
    public void paintComponent(Graphics graphics) {
        Graphics graphicComponent = graphics;

        graphicComponent.drawImage(this.imgBackground1, this.background1HorizontalPosition, 0, null);
        graphicComponent.drawImage(this.imgBackground2, this.background2HorizontalPosition, 0, null);
        graphicComponent.drawImage(this.imageStartCastle, 10 - this.xPos, 95, null);
        graphicComponent.drawImage(this.startArrow, 270 - this.xPos, 234, null);

        for (GameObject object : this.objects) {
            graphicComponent.drawImage(object.getImage(), object.getX(), object.getY(), null);
        }

        for (Coin coin : this.coins) {
            graphicComponent.drawImage(coin.imageOnMovement(), coin.getX(),coin.getY(), null);
        }

        graphicComponent.drawImage(this.imageFlag, FLAG_X_POS - this.xPos, FLAG_Y_POS, null);
        graphicComponent.drawImage(this.imageFinalCastle, CASTLE_X_POS - this.xPos, CASTLE_Y_POS, null);

        this.paintDynamicComponent(graphicComponent);

    }

    public void paintDynamicComponent(Graphics graphicComponent) {
        if (mario.isJumping())
            graphicComponent.drawImage(mario.doJump(), mario.getX(), mario.getY(), null);
        else
            graphicComponent.drawImage(mario.walkingImage(ResourceConstant.IMG_CHARACTER_MARIO, MARIO_FREQUENCY), this.mario.getX(), this.mario.getY(), null);

        if (mushroom.isAlive()) {
            graphicComponent.drawImage(mushroom.walkingImage(ResourceConstant.IMG_CHARACTER_MUSHROOM, MUSHROOM_FREQUENCY), mushroom.getX(), mushroom.getY(), null);
        } else {
            graphicComponent.drawImage(mushroom.changeImageWhenDie(), mushroom.getX(), mushroom.getY() + MUSHROOM_DEAD_OFFSET_Y, null);

        }

        if (turtle.isAlive()) {
            graphicComponent.drawImage(turtle.walkingImage(ResourceConstant.IMG_CHARACTER_TURTLE, TURTLE_FREQUENCY), turtle.getX(), turtle.getY(), null);

        } else {
            graphicComponent.drawImage(turtle.changeImageWhenDie(), turtle.getX(), turtle.getY() + TURTLE_DEAD_OFFSET_Y, null);

        }
        if (!this.mario.getAlive()) {
            graphicComponent.drawImage(ResourcesGetter.getImage(ResourceConstant.IMG_GAME_OVER), this.getMario().getX(), this.getMario().getY() / 2, null);
        }
        if (this.mario.getX() > FLAG_X_POS - this.xPos) {
            graphicComponent.drawImage(ResourcesGetter.getImage(ResourceConstant.IMG_GAME_WIN), this.getMario().getX(), this.getMario().getY() / 2, null);

        }
    }





}

