package gameModel;

/**
 * Created by Alessio Addimando on 12/03/2017.
 */
public interface GameElement {

    void setX(int x);

    void setY(int y);

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    void move();

}