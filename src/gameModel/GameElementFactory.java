package gameModel;

import gameModel.characters.*;
import gameModel.objects.Block;
import gameModel.objects.Coin;
import gameModel.objects.Tunnel;

/**
 * Created by Alessio Addimando on 12/03/2017.
 */
public interface GameElementFactory {

    Mushroom createMushroom(final int x,final int y);

    Turtle createTurtle(final int x,final int y);

    Tunnel createTunnel(final int x, final int y);

    Coin createCoin(final int x, final int y);

    Block createBlock(final int x, final int y);
}
