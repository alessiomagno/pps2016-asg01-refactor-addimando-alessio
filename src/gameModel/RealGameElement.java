package gameModel;

import gameController.GameLoopController;

/**
 * Created by Alessio Addimando on 12/03/2017.
 */

public class RealGameElement implements GameElement {

    protected int x;
    protected int y;
    protected int width;
    protected int height;


    public RealGameElement(int x, int y, int width, int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height= height;
    }


    public int getX() {
        return this.x;
    }


    public int getY() {
        return this.y;
    }


    public int getWidth() {return this.width;}


    public void setX(final int x) {
        this.x = x;
    }


    public void setY(final int y) {
        this.y = y;
    }


     public int getHeight() {
        return this.height;
    }


    public void move(){
        if(GameLoopController.getController().getScene().getHorizontalPosition() >= 0){
            this.x -= GameLoopController.getController().getScene().getMov();
        }
    }


}

