package gameModel.characters;

import gameController.GameLoopController;
import gameModel.GameElement;
import gameModel.objects.Block;
import gameModel.objects.Coin;
import utilities.ResourceConstant;
import utilities.ResourcesGetter;

import java.awt.*;

import static utilities.ResourceConstant.IMG_GOLDEN_BLOCK;
import static utilities.ResourceConstant.IMG_MARIO_SUPER_DX;


public class Mario extends BasicCharacter {

    private static final int MARIO_OFFSET_Y_INITIAL = 245;
    private static final int MARIO_OFFSET_X_INITIAL = 300;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;
    private static final int JUMPING_LIMIT = 42;
    private static final int JUMPING_SPEED = 4;

    private boolean jumping;
    private int jumpingExtent;
    private static Mario instance;

    public Mario() {
        super(MARIO_OFFSET_X_INITIAL, MARIO_OFFSET_Y_INITIAL, WIDTH, HEIGHT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public static Mario getInstance(){
        if(instance == null){
            instance = new Mario();
        }
        return instance;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public Image doJump() {
        String str;

        if (++this.jumpingExtent < JUMPING_LIMIT) {
            if (this.getY() > GameLoopController.getController().getScene().getHeightLimit())
                this.setY(this.getY() - JUMPING_SPEED);
            else{
                this.jumpingExtent = JUMPING_LIMIT;
            }

            str = this.isToRight() ? IMG_MARIO_SUPER_DX : ResourceConstant.IMG_MARIO_SUPER_SX;
        } else if (this.getY() + this.getHeight() < GameLoopController.getController().getScene().getDistanceVerticalFromFloor()) {
            this.setY(this.getY() + 1);
            str = this.isToRight() ? IMG_MARIO_SUPER_DX : ResourceConstant.IMG_MARIO_SUPER_SX;
        } else {
            str = this.isToRight() ? ResourceConstant.IMG_MARIO_ACTIVE_DX : ResourceConstant.IMG_MARIO_ACTIVE_SX;
            this.jumping = false;
            this.jumpingExtent = 0;
        }

        return ResourcesGetter.getImage(str);
    }

    @Override
    public void contactWithOtherComponent(GameElement component) {
        if(component instanceof EnemyCharacter) {
            EnemyCharacter enemy = (EnemyCharacter) component;
            if (this.hitAhead(component) || this.hitBack(component)) {
                if (((EnemyCharacter) component).isAlive()) {
                    this.setMoving(false);
                    this.setAlive(false);
                }else{
                    this.setAlive(true);
                }
            }else if (this.hitBelow(component)) {
                enemy.setMoving(false);
                enemy.setAlive(false);
            }
        }else{
            if (this.hitAhead(component) && this.isToRight() || this.hitBack(component) && !this.isToRight()) {
                this.setMoving(false);
                GameLoopController.getController().getScene().setMov(0);
            }

            if (this.hitBelow(component) && this.jumping) {
                GameLoopController.getController().getScene().setDistanceVerticalFromFloor(component.getY());

            } else if (!this.hitBelow(component)) {
                GameLoopController.getController().getScene().setDistanceVerticalFromFloor(FLOOR_OFFSET_Y_INITIAL);
                if (!this.jumping) {
                    this.setY(MARIO_OFFSET_Y_INITIAL);
                }

                if (hitAbove(component)) {
                    if(component instanceof Block){
                        ((Block) component).setImage(ResourcesGetter.getImage(IMG_GOLDEN_BLOCK));
                    }
                    GameLoopController.getController().getScene().setHeightLimit(component.getY() + component.getHeight());
                } else if (!this.hitAbove(component) && !this.jumping) {
                    GameLoopController.getController().getScene().setHeightLimit(0);
                }
            }
        }

    }

    public boolean contactCoin(Coin coin) {

        if (this.hitBack(coin) || this.hitAbove(coin) || this.hitAhead(coin)
                || this.hitBelow(coin))
            return true;

        return false;
    }

}
