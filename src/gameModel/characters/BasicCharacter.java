package gameModel.characters;

import gameModel.GameElement;
import gameModel.RealGameElement;
import utilities.ResourceConstant;
import utilities.ResourcesGetter;

import java.awt.*;

public abstract class BasicCharacter extends RealGameElement implements Character{

    public static final int PROXIMITY_MARGIN = 10;
    public static final int OBJECT_GAP = 5;

    private boolean moving;
    private boolean toRight;
    private int counter;
    private boolean alive;
    private boolean walkingFrameOn = true;

    public BasicCharacter(int x, int y, int width, int height) {
        super(x,y,width,height);
        this.counter = 0;
        this.moving = false;
        this.toRight = true;
        this.alive = true;
    }


    public boolean isAlive() {
        return alive;
    }

    public boolean isToRight() {
        return toRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }
    public void setToRight(boolean toRight) {
        this.toRight = toRight;
    }

    public boolean getAlive(){
        return this.alive;
    }

    public boolean hitAhead(GameElement component) {
        if (this.x + this.width < component.getX() ||
                this.x + this.width > component.getX() + OBJECT_GAP ||
                this.y + this.height <= component.getY() ||
                this.y >= component.getY() + component.getHeight()) {
            return false;
        }
        return true;
    }

    public boolean hitBack(GameElement object) {
        if (this.x > (object.getX() + object.getWidth()) ||
                this.x + this.width < object.getX() + object.getWidth() - OBJECT_GAP ||
                this.y + this.height <= object.getY() ||
                this.y >= object.getY() + object.getHeight()){
            return false;
        }
        return true;
    }

    public boolean hitBelow(GameElement object) {
        if (this.x + this.width < object.getX() + OBJECT_GAP ||
                this.x > object.getX() + object.getWidth() - OBJECT_GAP ||
                this.y + this.height < object.getY() ||
                this.y + this.height > object.getY() + OBJECT_GAP) {
            return false;
        }
        return true;
    }

    public boolean hitAbove(GameElement component) {
        if (this.x + this.width < component.getX() + OBJECT_GAP ||
                this.x > component.getX() + component.getWidth() - OBJECT_GAP ||
                this.y < component.getY() + component.getHeight() ||
                this.y > component.getY() + component.getHeight() + OBJECT_GAP) {
            return false;

        }
        return true;
    }

    public boolean isNearToComponent(GameElement component) {
        if ((this.x > component.getX() - PROXIMITY_MARGIN && this.x < component.getX() + component.getWidth() + PROXIMITY_MARGIN)
                || (this.x + this.width > component.getX() - PROXIMITY_MARGIN
                && this.x + this.width < component.getX() + component.getWidth() + PROXIMITY_MARGIN)){
            return true;
        }
        return false;
    }


    public Image walkingImage(String name, int frequency) {
        if (++this.counter % frequency==0){
            walkingFrameOn =!walkingFrameOn;
        }
        String str = ResourceConstant.IMG_BASE + name +
                (!this.moving || walkingFrameOn? ResourceConstant.IMG_STATUS_ACTIVE : ResourceConstant.IMG_STATUS_NORMAL)+
                (this.toRight ? ResourceConstant.IMG_DIRECTION_DX : ResourceConstant.IMG_DIRECTION_SX) + ResourceConstant.IMG_EXT;
        return ResourcesGetter.getImage(str);
    }

    public abstract void contactWithOtherComponent(GameElement component);

}
