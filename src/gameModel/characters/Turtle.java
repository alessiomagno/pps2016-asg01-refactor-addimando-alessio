package gameModel.characters;

import utilities.ResourceConstant;
import utilities.ResourcesGetter;

import java.awt.*;

public class Turtle extends EnemyCharacter{

    public static final int WIDTH = 43;
    public static final int HEIGHT = 50;


    public Turtle(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.setToRight(true);
        super.setMoving(true);

        Thread turtleThread = new Thread(this);
        turtleThread.start();
    }
    @Override
    public Image getImageOfDeath() {

        return ResourcesGetter.getImage(ResourceConstant.IMG_TURTLE_DEAD);
    }
}
