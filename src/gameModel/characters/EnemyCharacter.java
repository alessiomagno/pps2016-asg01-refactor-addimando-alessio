package gameModel.characters;

import gameModel.GameElement;
import java.awt.*;


/**
 * Created by Alessio Addimando on 12/03/2017.
 */
public abstract class EnemyCharacter extends BasicCharacter implements Runnable{

    private final int PAUSE = 25;
    private int offsetX;

    public EnemyCharacter(int x, int y, int width, int height) {
        super(x, y, width, height);
        super.setToRight(true);
        super.setMoving(true);
        this.offsetX = 1;
    }

    @Override
    public void move(){
            this.offsetX = isToRight() ? 1 : -1;
            this.setX(this.getX() + this.offsetX);

    }

    @Override
    public void run() {
        while(this.isAlive()){
                this.move();
                try{
                    Thread.sleep(PAUSE);
                } catch(InterruptedException e){

                }
            }

    }

    public void contactWithOtherComponent(GameElement component) {
        if (this.hitAhead(component) && this.isToRight()) {
            this.setToRight(false);
            this.offsetX = -1;
        } else if (this.hitBack(component) && !this.isToRight()) {
            this.setToRight(true);
            this.offsetX = 1;
        }
    }

    public Image changeImageWhenDie(){
        return getImageOfDeath();
    }

    public abstract Image getImageOfDeath();

}
