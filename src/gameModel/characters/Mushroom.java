package gameModel.characters;

import utilities.ResourceConstant;
import utilities.ResourcesGetter;

import java.awt.*;

public class Mushroom extends EnemyCharacter implements Runnable {

    public static final int WIDTH = 15;
    public static final int HEIGHT = 30;


    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.setToRight(true);
        this.setMoving(true);

        Thread mushroomThread = new Thread(this);
        mushroomThread.start();
    }

    @Override
    public Image getImageOfDeath() {
        return ResourcesGetter.getImage(this.isToRight() ? ResourceConstant.IMG_MUSHROOM_DEAD_DX : ResourceConstant.IMG_MUSHROOM_DEAD_SX);
    }
}
