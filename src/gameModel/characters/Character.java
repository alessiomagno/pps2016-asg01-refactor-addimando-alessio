package gameModel.characters;

import gameModel.GameElement;

import java.awt.Image;

/**
 * this interface represents the behaviour of a gameController's character
 */

public interface Character{

   boolean isAlive();

    boolean isToRight();

    void setAlive(boolean alive);

    void setMoving(boolean moving);

    void setToRight(boolean toRight);

    Image walkingImage(String name, int frequency);//

    boolean isNearToComponent(GameElement component);//

    void contactWithOtherComponent(GameElement component);//

    void move();
}
