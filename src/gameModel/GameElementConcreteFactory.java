package gameModel;

import gameModel.characters.Mushroom;
import gameModel.characters.Turtle;
import gameModel.objects.Block;
import gameModel.objects.Coin;
import gameModel.objects.Tunnel;

/**
 * Created by Alessio Addimando on 12/03/2017.
 */
public class GameElementConcreteFactory implements GameElementFactory {


    private static GameElementConcreteFactory instance;

    public static GameElementConcreteFactory getInstance(){
        if(instance == null){
            instance = new GameElementConcreteFactory();
        }
        return instance;
    }
    @Override
    public Mushroom createMushroom(int x, int y) {
        return new Mushroom(x,y);
    }

    @Override
    public Turtle createTurtle(int x, int y) {
        return new Turtle(x,y);
    }

    @Override
    public Tunnel createTunnel(int x, int y) {
        return new Tunnel(x,y);
    }

    @Override
    public Coin createCoin(int x, int y) {
        return new Coin(x,y);
    }

    @Override
    public Block createBlock(int x, int y) {
        return new Block(x,y);
    }
}


