package gameModel.objects;

import utilities.ResourceConstant;
import utilities.ResourcesGetter;

import java.awt.*;

public class Coin extends RealGameObject implements Runnable {

    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;
    private static final int PAUSE = 10;
    public static final int FLIP_FREQUENCY = 100;
    private int counter = 0;
    private boolean coinShowed = true;

    public Coin(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.setImage(ResourcesGetter.getImage(ResourceConstant.IMG_PIECE1));
    }

    public Image imageOnMovement() {
        if(++this.counter % FLIP_FREQUENCY ==  0){
            this.coinShowed = !this.coinShowed;
        }
        return ResourcesGetter.getImage(coinShowed? ResourceConstant.IMG_PIECE1: ResourceConstant.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }


}
