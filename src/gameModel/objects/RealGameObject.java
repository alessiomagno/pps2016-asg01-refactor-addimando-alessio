package gameModel.objects;

import gameModel.RealGameElement;

import java.awt.*;

public class RealGameObject extends RealGameElement implements GameObject{

    private Image image;

    public RealGameObject(int x, int y, int width, int height) {
       super(x,y,width,height);
    }

    @Override
    public Image getImage() {
        return this.image;
    }

    @Override
    public void setImage(Image image) {
        this.image = image;
    }
}
