package gameModel.objects;

import utilities.ResourceConstant;
import utilities.ResourcesGetter;

public class Tunnel extends RealGameObject {

    public static final int WIDTH = 43;
    public static final int HEIGHT = 65;


    public Tunnel(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        super.setImage(ResourcesGetter.getImage(ResourceConstant.IMG_TUNNEL));
    }

}
