package gameModel.objects;

import gameModel.GameElement;
import java.awt.*;

/**
 * Created by Alessio Addimando on 12/03/2017.
 */

public interface GameObject extends GameElement{

    Image getImage();

    void setImage(Image image);

}