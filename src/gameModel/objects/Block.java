package gameModel.objects;

import utilities.ResourceConstant;
import utilities.ResourcesGetter;

public class Block extends RealGameObject {

    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;

    public Block(int x, int y) {

        super(x, y, WIDTH, HEIGHT);
        super.setImage( ResourcesGetter.getImage(ResourceConstant.IMG_BLOCK));
    }

}
